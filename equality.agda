module equality where


infix 5 _≡_
data _≡_ {A : Set} (x : A) : A → Set where
  refl : x ≡ x
  
≡-assoc : {A : Set} {p q : A} → p ≡ q → q ≡ p
≡-assoc refl = refl 

≡-trans : {A : Set} {p q r : A} → p ≡ q → q ≡ r → p ≡ r
≡-trans refl z = z



cong : ∀ {A : Set} (f : A → A) {x y} → x ≡ y → f x ≡ f y
cong f refl = refl 
