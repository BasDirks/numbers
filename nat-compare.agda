module nat-compare where


open import bottom
open import equality
open import nat


data _≤_ : ℕ → ℕ → Set where
  O≤ : ∀ b → O ≤ b
  S≤ : ∀ a b → a ≤ b → S a ≤ S b
  
a≤a : ∀ a → a ≤ a
a≤a O = O≤ O
a≤a (S a) = S≤ a a (a≤a a)

a≡b→a≤b : ∀ a b → a ≡ b → a ≤ b
a≡b→a≤b .b b refl = a≤a b

Sa≤Sb→a≤b : ∀ a b → S a ≤ S b → a ≤ b
Sa≤Sb→a≤b a b (S≤ .a .b p) = p 



_≥_ : ℕ → ℕ → Set
a ≥ b = b ≤ a
  
a≥a : ∀ a → a ≥ a
a≥a O = O≤ O
a≥a (S a) = S≤ a a (a≥a a) 

a≡b→a≥b : ∀ a b → a ≡ b → a ≥ b
a≡b→a≥b .b b refl = a≥a b

Sa≥Sb→a≥b : ∀ a b → S a ≥ S b → a ≥ b 
Sa≥Sb→a≥b a b (S≤ .b .a p) = p 



_>_ : ℕ → ℕ → Set
a > b = b ≤ a



_<_ : ℕ → ℕ → Set
a < b = S a > b
