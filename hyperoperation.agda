module hyperoperation where


open import bottom
open import equality
open import nat
open import nat-compare


-- Hyperoperation sequence, Goodstein's notation. 
G : ℕ → ℕ → ℕ → ℕ 
G O             _ b     = S b
G 1             a O     = a
G 2             _ O     = O
G (S (S (S _))) _ O     = 1
G (S n)         a (S b) = G n a (G (S n) a b)



infixl 6 _Sᴳ_
_Sᴳ_ : ℕ → ℕ → ℕ
_Sᴳ_ = G O

aSᴳb≡Sb : ∀ {a b} → a Sᴳ b ≡ S b
aSᴳb≡Sb = refl 



infixl 6 _+ᴳ_
_+ᴳ_ : ℕ → ℕ → ℕ
_+ᴳ_ = G 1 

+ᴳ1≡S : ∀ {a} → a +ᴳ 1 ≡ S a
+ᴳ1≡S = refl 

Sa+ᴳb≡a+ᴳSb : ∀ a b → S a +ᴳ b ≡ a +ᴳ S b
Sa+ᴳb≡a+ᴳSb a O     = refl
Sa+ᴳb≡a+ᴳSb a (S b) = cong S (Sa+ᴳb≡a+ᴳSb a b)

+ᴳ-rid : ∀ {a} → a +ᴳ O ≡ a
+ᴳ-rid = refl

+ᴳ-lid : ∀ b → O +ᴳ b ≡ b
+ᴳ-lid O     = refl
+ᴳ-lid (S b) = cong S (+ᴳ-lid b)

+ᴳ-assoc : ∀ a b c → (a +ᴳ b) +ᴳ c ≡ a +ᴳ (b +ᴳ c)
+ᴳ-assoc a b O     = refl
+ᴳ-assoc a b (S c) = cong S (+ᴳ-assoc a b c)

+ᴳ-comm : ∀ a b → a +ᴳ b ≡ b +ᴳ a
+ᴳ-comm O O = refl
+ᴳ-comm O (S b) = cong S (+ᴳ-comm O b)
+ᴳ-comm (S a) O = cong S (+ᴳ-comm a O)
+ᴳ-comm (S a) (S b) = cong S {!!} 



infixl 7 _×ᴳ_
_×ᴳ_ : ℕ → ℕ → ℕ
_×ᴳ_ = G 2 

a+ᴳa≡a×ᴳ2 : ∀ {a} → a +ᴳ a ≡ a ×ᴳ 2
a+ᴳa≡a×ᴳ2 = refl

a×ᴳO≡O : ∀ {a} → a ×ᴳ O ≡ O 
a×ᴳO≡O = refl

O×ᴳb≡O : ∀ b → O ×ᴳ b ≡ O 
O×ᴳb≡O O = refl
O×ᴳb≡O (S b) = {!!}

×ᴳ-rid : ∀ {a} → a ×ᴳ 1 ≡ a
×ᴳ-rid = refl 



infixl 8 _^ᴳ_
_^ᴳ_ : ℕ → ℕ → ℕ
_^ᴳ_ = G 3

a×ᴳa≡a^ᴳ2 : ∀ {a} → a ×ᴳ a ≡ a ^ᴳ 2
a×ᴳa≡a^ᴳ2 = refl 

b^ᴳO≡b : ∀ {b} → b ^ᴳ O ≡ 1
b^ᴳO≡b = refl 

b^ᴳ1≡b : ∀ {b} → b ^ᴳ 1 ≡ b
b^ᴳ1≡b = refl 



infixl 9 _↑ᴳ_
_↑ᴳ_ : ℕ → ℕ → ℕ
_↑ᴳ_ = G 4



infixl 10 _↑↑ᴳ_
_↑↑ᴳ_ : ℕ → ℕ → ℕ
_↑↑ᴳ_ = G 5



infixl 11 _↑↑↑ᴳ_
_↑↑↑ᴳ_ : ℕ → ℕ → ℕ
_↑↑↑ᴳ_ = G 6



-- Original Ackermann function.
φ : ℕ → ℕ → ℕ → ℕ
φ m n     O     = m +ᴳ n
φ _ O     1     = O
φ _ O     2     = 1
φ m O     _     = m
φ m (S n) (S p) = φ m (φ m n (S p)) p

φabO≡G1ab : ∀ {a b} → φ a b O ≡ G 1 a b
φabO≡G1ab = refl

φ1aO≡G2aO : ∀ {a} → φ a O 1 ≡ G 2 a O
φ1aO≡G2aO = refl



-- "The" two-argument Ackermann function by Péter and Robinson.
A : ℕ → ℕ → ℕ
A O     b     = S b
A (S a) O     = A a 1
A (S a) (S b) = A a (A (S a) b)

AOb≡aSᴳb : ∀ {a b} → A O b ≡ a Sᴳ b
AOb≡aSᴳb = refl

A1b≡2+ᴳb : ∀ b → A 1 b ≡ 2 +ᴳ b
A1b≡2+ᴳb O     = refl
A1b≡2+ᴳb (S b) = cong S (A1b≡2+ᴳb b) 



-- Single-argument Ackermann function.
A₂ : ℕ → ℕ → ℕ
A₂ O     = S
A₂ (S a) = g (A₂ a)
  where g : (ℕ → ℕ) → ℕ → ℕ
        g f O = f 1
        g f (S b) = f (g f b)



-- unproven
-- A2n≡3+ᴳ[O×ᴳn] : ∀ n → A 2 n ≡ (n ×ᴳ 2) +ᴳ 3

-- O×ᴳn≡O : ∀ n → O ×ᴳ n ≡ O 
-- O^ᴳn≡O : ∀ {n} → O ^ᴳ n ≡ O
-- 1^ᴳn≡1 : ∀ n → 1 ^ᴳ n ≡ 1
-- ×ᴳ-assoc : ∀ a b c → (a ×ᴳ b) ×ᴳ c ≡ a ×ᴳ (b ×ᴳ c)
-- nGᵐ⁺¹n≡nGᵐ⁺²2 : ∀ m n → G (S m) n n ≡ G (S (S m)) n 2
