module nat where


open import bottom
open import equality


data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}
{-# BUILTIN ZERO    O #-}
{-# BUILTIN SUC     S #-}

a≡b→Sa≡Sb : ∀ {a b} → a ≡ b → S a ≡ S b
a≡b→Sa≡Sb refl = refl 

Sa≡O→⊥ : ∀ {a} → S a ≡ O → ⊥
Sa≡O→⊥ ()
